﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }

        private void FillData()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        //编辑
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }

        //添加/更新
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var id = GetValue(e.RowIndex, 0).Replace("&nbsp;", "");
            var title = GetValue(e.RowIndex, 1);
            var content = GetValue(e.RowIndex, 2);
            var author = GetValue(e.RowIndex, 3);

            if (string.IsNullOrEmpty(id))
            {
                var sql = string.Format("insert into Articles (Title,Content,Author) values ('{0}','{1}','{2}')", title, content, author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}' where Id={3}", title, content, author, id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            GridView1.EditIndex = -1;
            FillData();
        }

        private string GetValue(int rowIndex, int colIndex)
        {
            var control = GridView1.Rows[rowIndex].Cells[colIndex];
            var txt = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return txt;
        }

        //删除
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = GetValue(e.RowIndex, 0);
            var sql = string.Format("delete from Articles where Id={0}", id);
            DbHelper.AddOrUpdateOrDelete(sql);
            FillData();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));

            foreach (GridViewRow row in GridView1.Rows)
            {
                var item = dt.NewRow();
                item["Id"] = GetValue(row.RowIndex, 0);
                item["Title"] = GetValue(row.RowIndex, 1);
                item["Content"] = GetValue(row.RowIndex, 2);
                item["Author"] = GetValue(row.RowIndex, 3);

                dt.Rows.Add(item);
            }
            var tempItem = dt.NewRow();
            dt.Rows.Add(tempItem);

            GridView1.DataSource = dt;
            GridView1.DataBind();

            GridView1.EditIndex = GridView1.Rows.Count - 1;

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}