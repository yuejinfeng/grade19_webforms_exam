﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        private  void FillData()
        {
            var sql = string.Format("select * from Articles");
            var dt = DbHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        private string GetValue(int rowIndex,int colIndex)
        {
            var contorl = GridView1.Rows[rowIndex].Cells[colIndex];
            var txt = contorl.Controls.Count > 0 ? ((TextBox)contorl.Controls[0]).Text : contorl.Text;
            return txt;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));
            dt.Columns.Add(new DataColumn("Id"));
            foreach(GridViewRow row in GridView1.Rows)
            {
                var item = dt.NewRow();
                item["Title"] = GetValue(row.RowIndex, 1);
                item["Content"] = GetValue(row.RowIndex, 2);
                item["Author"] = GetValue(row.RowIndex, 3);
                item["Id"] = GetValue(row.RowIndex, 0);
                dt.Rows.Add(item);
            }
            var tempItme = dt.NewRow();
            dt.Rows.Add(tempItme);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.EditIndex = dt.Rows.Count - 1;
            GridView1.DataSource = dt;
            GridView1.DataBind();

        }


        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id =int.Parse( GetValue(e.RowIndex, 0));
            var sql = string.Format("delete from Articles where Id={0}", id);
            DbHelper.AddOrUpdateOrDelete(sql);
            FillData();
            GridView1.EditIndex = -1;
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)

        {
            var tempValue = GetValue(e.RowIndex, 0);
            var isOk = int.TryParse(tempValue, out int Id);
            var Title = GetValue(e.RowIndex, 1);
            var Content = GetValue(e.RowIndex, 2);
            var Author = GetValue(e.RowIndex, 3);
            
            if(isOk)
            {
                var sql = string.Format("update  Articles set Title='{0}',Content='{1}',Author='{2}' where Id={3}", Title, Content, Author, Id);
                DbHelper.AddOrUpdateOrDelete(sql);
               
            }
            else
            {
                var sql = string.Format("insert into Articles(Title, Content, Author) Values('{0}','{1}','{2}')", Title, Content, Author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            GridView1.EditIndex = -1;
            FillData();
        }
    }
}