﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        private void FillData()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);

            Gv1.DataSource = dt;
            Gv1.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                FillData();
            }
        }

        private string GetValues(int rowIndex,int CelIndex)
        {
            var control = Gv1.Rows[rowIndex].Cells[CelIndex];

            var txt = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;

            return txt;
        }

        private DataTable Clone()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));
            dt.Columns.Add(new DataColumn("IsActived"));
            dt.Columns.Add(new DataColumn("IsDelted"));
            dt.Columns.Add(new DataColumn("CreatedTime"));
            dt.Columns.Add(new DataColumn("UpdatedTime"));
            dt.Columns.Add(new DataColumn("Remarks"));

            foreach (GridViewRow row in Gv1 .Rows )
            {
                var tempRow = dt.NewRow();

                tempRow["Id"] = GetValues(row.RowIndex, 0);
                tempRow["Title"] = GetValues(row.RowIndex, 1);
                tempRow["Content"] = GetValues(row.RowIndex, 2);
                tempRow["Author"] = GetValues(row.RowIndex, 3);
                tempRow["IsActived"] = GetValues(row.RowIndex, 4);
                tempRow["IsDelted"] = GetValues(row.RowIndex, 5);
                tempRow["CreatedTime"] = GetValues(row.RowIndex, 6);
                tempRow["UpdatedTime"] = GetValues(row.RowIndex, 7);
                tempRow["Remarks"] = GetValues(row.RowIndex, 8);

                dt.Rows.Add(tempRow);
            }

            dt.Rows.Add(dt.NewRow());

            return dt;
        }

        protected void Gv1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Gv1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void Gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            Gv1.EditIndex = -1;
            FillData();
        }

        protected void Gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var title = GetValues(e.RowIndex, 1);
            var content = GetValues(e.RowIndex, 2);
            var author = GetValues(e.RowIndex, 3);
            var actived = GetValues(e.RowIndex, 4);
            var delted = GetValues(e.RowIndex, 5);
            var created = GetValues(e.RowIndex, 6);
            var updated = GetValues(e.RowIndex, 7);
            var remarks = GetValues(e.RowIndex, 8);

            var tempValue = GetValues(e.RowIndex, 0);

            var isOk = int.TryParse(tempValue, out int Id);

            if(isOk)
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}',IsActived='{3}',IsDelted='{4}',CreatedTime='{5}',UpdatedTime='{6}',Remarks='{7}' where Id={8}", title, content, author, actived, delted, created, updated, remarks, Id);

                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("insert into Articles(Title,Content,Author,IsActived,IsDelted,CreatedTime,UpdatedTime,Remarks)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", title, content, author, actived, delted, created, updated, remarks);

                DbHelper.AddOrUpdateOrDelete(sql);
            }

            Gv1.EditIndex = -1;
            FillData();
        }

        protected void Gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var Id = int.Parse(GetValues(e.RowIndex, 0));

            var sql = string.Format("delete from Articles where Id={0}", Id);

            DbHelper.AddOrUpdateOrDelete(sql);

            Gv1.EditIndex = -1;
            FillData();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
             var dataTable = Clone();

            Gv1.DataSource = dataTable;
            Gv1.DataBind();

            Gv1.EditIndex = dataTable.Rows.Count - 1;

            Gv1.DataSource = dataTable;
            Gv1.DataBind();
        }
    }
}