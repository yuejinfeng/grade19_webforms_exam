﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style >
        th {
            text-align :center ;
            width :800px;
        }
    </style>
    <asp:Button runat ="server" Text ="添加" OnClick ="Unnamed_Click" />
<asp:GridView runat ="server" ID ="gvl" AutoGenerateColumns ="false" OnRowCancelingEdit ="gvl_RowCancelingEdit" OnRowDeleting ="gvl_RowDeleting" OnRowEditing ="gvl_RowEditing" OnRowUpdating ="gvl_RowUpdating" >
    <Columns >
        <asp:BoundField DataField ="Id" HeaderText ="Id" ReadOnly ="true" />
        <asp:BoundField DataField ="Title" HeaderText ="标题" />
        <asp:BoundField DataField ="Content" HeaderText ="内容" />
        <asp:BoundField DataField ="Author" HeaderText ="作者" />
        <asp:CommandField ButtonType ="Button" HeaderText ="操作" ShowEditButton ="true" ShowDeleteButton ="true" />
    </Columns>
    <RowStyle HorizontalAlign ="Center" />
</asp:GridView>

</asp:Content>
