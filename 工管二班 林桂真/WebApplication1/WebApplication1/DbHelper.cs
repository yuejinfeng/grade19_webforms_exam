﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class DbHelper
    {
        private static string conString = "server=.;database=CMS;uid=sa;pwd=123456;";
        public static DataTable GetDataTable(string sql)
        {
            var adapter = new SqlDataAdapter(sql, conString);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }

        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            var count = command.ExecuteNonQuery();
            connection.Close();
            return count;
        }
    }
}