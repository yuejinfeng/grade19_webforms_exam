﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                FillData();
            }
        }
        //取消编辑
        protected void GridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView.EditIndex = -1;
            FillData();

        }
        //删除
        protected void GridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GetValue(e.RowIndex, 0));
            var sql = string.Format("delete from Articles where id={0}", id);
            DbHelper.AddOrUpdateOrDelete(sql);
            GridView.EditIndex = -1;
            FillData();

        }
        //编辑
        protected void GridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView.EditIndex = e.NewEditIndex;
            FillData();
        }
        //更新
        protected void GridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var id = GetValue(e.RowIndex, 0).Replace("&nbsp","");
            var Title = GetValue(e.RowIndex, 1);
            var Content = GetValue(e.RowIndex, 2);
            var Author = GetValue(e.RowIndex, 3);
            if (string.IsNullOrEmpty(id))
            {
                var sql = string.Format("insert into Articles(Title,Content,Author)values('{0}','{1}','{2}')", Title, Content, Author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {

                var sql = string.Format("update Articles set Title='{0}', Content='{1}',Author='{2}' where id={3}", Title, Content, Author, id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            GridView.EditIndex = -1;
            FillData();
        }
        //克隆
        protected void Unnamed_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));

            foreach (GridViewRow row in GridView.Rows)
            {
                var Temp = dt.NewRow();
                Temp["id"] = GetValue(row.RowIndex, 0);
                Temp["Title"] = GetValue(row.RowIndex, 1);
                Temp["Content"] = GetValue(row.RowIndex, 2);
                Temp["Author"] = GetValue(row.RowIndex, 3);
                dt.Rows.Add(Temp);

            }
            var te = dt.NewRow();
            dt.Rows.Add(te);
            GridView.DataSource = dt;
            GridView.DataBind();
            GridView.EditIndex = dt.Rows.Count - 1;
            GridView.DataSource = dt;
            GridView.DataBind();
        }
        //获取数据库
        private void FillData()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);
            GridView.DataSource = dt;
            GridView.DataBind();

        }
        //获取引用单元格
        private string GetValue(int rowIndex, int colIndex)
        {
            var control = GridView.Rows[rowIndex].Cells[colIndex];
            var tex = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return tex;
        }
    }
}