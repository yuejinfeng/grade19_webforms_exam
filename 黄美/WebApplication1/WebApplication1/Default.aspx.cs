﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack )
            {
                FillData();
            }
        }
        private void FillData()
        {
            var sql = "select * from Articles";
            var  dt=DbHelper.GetDataTable(sql);
            gv1.DataSource = dt;
            gv1.DataBind();
        }

        protected void gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GrtValue(e.RowIndex, 0));
            var sql = string.Format("delete from Articles where Id={0}", id);
            DbHelper.AddOrUpdateOrDelete(sql);
            gv1.EditIndex = -1;
            FillData();
        }

        private string GrtValue(int rowIndex, int colIndex)
        {
            var cont = gv1.Rows[rowIndex].Cells[colIndex];
            var value = cont.Controls.Count > 0 ? ((TextBox)cont.Controls[0]).Text : cont.Text;
            return value;
        }

        protected void gv1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv1.EditIndex = e.NewEditIndex;
            gv1.EditIndex = -1;
        }

        protected void gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var Title = GrtValue(e.RowIndex, 1);
            var Content = GrtValue(e.RowIndex, 2);
            var Author = GrtValue(e.RowIndex,3);
            var temp = GrtValue(e.RowIndex,0);
            var dgj = int.TryParse(temp, out int id);

            if (dgj)
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}' where Id={3}", Title, Content, Author, id);
                DbHelper.GetDataTable(sql);
            }
            else
            {
                var sql = string.Format("insert into Articles (Title,Content,Author) values ('{0}','{1}','{2}')", Title, Content, Author);
                DbHelper.GetDataTable(sql);
            }
           
            gv1.EditIndex = -1;
            FillData();
        }

        protected void gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv1.EditIndex = -1;
            FillData();
        }

        private DataTable ColneDataDromGridviem()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));

            foreach (GridViewRow row in gv1.Rows)
            {
                var tempvalue = dt.NewRow();
                tempvalue["Id"] = GrtValue(row.RowIndex, 0);
                tempvalue["Title"] = GrtValue(row.RowIndex, 1);
                tempvalue["Content"] = GrtValue(row.RowIndex, 2);
                tempvalue["Author"] = GrtValue(row.RowIndex, 3);

                dt.Rows.Add(tempvalue);
            }
            dt.Rows.Add(dt.NewRow());

            return dt;
        }


        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var dataTable = ColneDataDromGridviem();
            gv1.DataSource = dataTable;
            gv1.DataBind();

            gv1.EditIndex = dataTable.Rows.Count - 1;
            gv1.DataSource = dataTable;
            gv1.DataBind();
        }
    }
   
}