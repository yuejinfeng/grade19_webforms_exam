﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style >
        th{
            text-align :center ;
            width :180px;
            height :40px
        }
        td{
            text-align :center;
            width :180px;
            height :40px
        }
    </style>
    <asp:Button runat ="server" Text ="新增" OnClick ="Unnamed_Click" Height="43px" Width="67px" />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns ="False" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" CellPadding="4" ForeColor="#333333" GridLines="None"  >
        <AlternatingRowStyle BackColor="White" />
        <Columns >
            <asp:BoundField DataField ="Id" ReadOnly ="true" HeaderText ="Id" />
            <asp:BoundField DataField ="Title" HeaderText ="标题" />
            <asp:BoundField DataField ="Content" HeaderText ="内容" />
            <asp:BoundField DataField ="Author" HeaderText ="作者" />
            <asp:CommandField HeaderText ="操作" ShowDeleteButton ="true" ShowEditButton ="true" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>

</asp:Content>
