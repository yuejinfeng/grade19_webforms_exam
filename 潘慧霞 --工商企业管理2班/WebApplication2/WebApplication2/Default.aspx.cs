﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1;

namespace WebApplication2
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack )
            {
                FillData();
            }
        }

        private void  FillData()
        {
            var sql = "SELECT * FROM Articles ";
            var dt = DbHelper.GetDataTable(sql);
            gv1.DataSource = dt;
            gv1.DataBind();
        }
        private string GetValues(int rowIndex ,int colIndex)
        {
            var count = gv1.Rows[rowIndex].Cells[colIndex ];
            var temper = count.Controls.Count > 0 ? ((TextBox)count.Controls[0]).Text : count.Text;
            return temper;
        }
        protected void gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv1.EditIndex = -1;
            FillData();
        }

        protected void gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GetValues (e.RowIndex ,0));
            var sql = string.Format(" delete from Articles where id  = {0}" ,id );
            var dt = DbHelper.GetDataTable(sql);
            gv1.DataSource = dt;
            gv1.EditIndex = -1;
            FillData();
        }

        protected void gv1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gv1.EditIndex = e.NewEditIndex;
            FillData();
        }
        private DataTable GetValuesFromGeriVidw()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn ("Id"));
            dt.Columns.Add(new DataColumn  ("Title"));
            dt.Columns.Add(new DataColumn ("Content"));
            dt.Columns.Add(new DataColumn ("Author"));
            foreach (GridViewRow  row in gv1 .Rows )
            {
                var temper = dt.NewRow();
                temper["Id"] = GetValues(row .RowIndex ,0);
                temper["Title"] = GetValues(row .RowIndex ,1);
                temper["Content"] = GetValues(row .RowIndex ,2);
                temper["Author"] = GetValues(row .RowIndex ,3);
                dt.Rows.Add(temper );
            }
            dt.Rows.Add(dt.NewRow ());
            gv1.EditIndex = -1;
            return dt;
        }
        protected void gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var tmeper = GetValues(e.RowIndex ,0);
            var isok = int.TryParse(tmeper ,out int id );
            var Title = GetValues(e.RowIndex, 1); ;
            var Content = GetValues(e.RowIndex ,2);
            var Author = GetValues(e.RowIndex ,3);
            if (isok)
            {
                var sql = string.Format(" update Articles  set Title = '{0}' ,Content = '{1}' ,Author ='{2}'  WHERE Id = {3} ", Title, Content, Author, id);
                DbHelper.AddOrUpdateOrDelete(sql);

            }
            else
            {
                var sql = string.Format(" insert into Articles  (Title ,Content ,Author ) values ('{0}','{1}','{2}')" , Title ,Content ,Author );
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            gv1.EditIndex = -1;
            FillData();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var temper = GetValuesFromGeriVidw();
            gv1.DataSource = temper;
            gv1.DataBind();
            gv1.EditIndex = temper.Rows.Count - 1;
            gv1.DataSource = temper;
            gv1.DataBind();
        }
    }
}