﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Button  runat="server" ID="ubtAdd"  Text="新增" OnClick="ubtAdd_Click"/>
<asp:GridView ID="GridView1" AutoGenerateColumns="false" runat="server" OnRowEditing="GridView1_RowEditing" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowUpdating="GridView1_RowUpdating">
    <Columns>
        <asp:BoundField  DataField="Id" HeaderText="Id" ReadOnly="true"/>
        <asp:BoundField  DataField="Title" HeaderText="加标题"/>
        <asp:BoundField  DataField="Content" HeaderText="内容"/>
        <asp:BoundField  DataField="Author" HeaderText="作者"/>
        <asp:CommandField  HeaderText="操作" ShowCancelButton="true" ShowDeleteButton="true" ShowEditButton="true" ShowHeader="true"/>
    </Columns>
</asp:GridView>

</asp:Content>
