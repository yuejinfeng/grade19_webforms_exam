﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                FillData();
            }
        }
        private void FillData() 
        {
            var sql = string.Format("Select * from Articles");
            var dt = DbHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        private string GetValues(int RowIndex, int LineIndex)
        {
            var control = GridView1.Rows[RowIndex].Cells[LineIndex];
            var txt = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return txt;
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var Id = int.Parse(GetValues(e.RowIndex, 0));
            var sql = string.Format("delete from Articles where id = '{0}'",Id);
            DbHelper.AddOrUpdateOrDelete(sql);
            FillData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var id = GetValues(e.RowIndex, 0).Replace("&nbsp;","");
            var Title = GetValues(e.RowIndex, 1);
            var Content = GetValues(e.RowIndex, 2);
            var Author = GetValues(e.RowIndex, 3);
            if (string.IsNullOrEmpty(id))
            {
                var sql = string.Format(" insert into Articles (Title,Content,Author) Values ('{0}','{1}','{2}')", Title, Content, Author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else 
            {
                var sql = string.Format("update Articles set Title = '{0}', Content = '{1}', Author = '{2}' where id = {3}", Title, Content, Author, id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void ubtAdd_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("Title");
            dt.Columns.Add("Content");
            dt.Columns.Add("Author");
            foreach (GridViewRow row in GridView1.Rows)
            {
                var tempValue = dt.NewRow();
                tempValue["Id"] = GetValues(row.RowIndex, 0);
                tempValue["Title"] = GetValues(row.RowIndex, 1);
                tempValue["Content"] = GetValues(row.RowIndex, 2);
                tempValue["Author"] = GetValues(row.RowIndex, 3);
                dt.Rows.Add(tempValue);
            }
            var tempInset = dt.NewRow();
            dt.Rows.Add(tempInset);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.EditIndex = dt.Rows.Count - 1; 
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}