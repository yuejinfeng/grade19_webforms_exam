﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <style>
        th {
            text-align:center;
            width:80px;

        }
        td {

            text-align:center;
            
        }


    </style>
   
  <asp:GridView runat="server" Id="gv1" AutoGenerateColumns="false" OnRowDeleting="gv1_RowDeleting" OnRowUpdating="gv1_RowUpdating" OnRowEditing="gv1_RowEditing" 
      OnRowCancelingEdit="gv1_RowCancelingEdit">
      
      
      <Columns>
           <asp:BoundField DataField="Id" HeaderText="Id" />
          <asp:BoundField DataField="Title" HeaderText="标题" />
          <asp:BoundField DataField="Content" HeaderText="内容" />
          <asp:BoundField DataField="Author" HeaderText="作者" />

          <asp:CommandField ShowEditButton="true" ShowDeleteButton="true" HeaderText="操作" />


      </Columns>

  </asp:GridView>

</asp:Content>
