﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        //调用数据库
        private void FillData()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        private string GetValues(int rowIndex,int conIndex)
        {
            var control = GridView1.Rows[rowIndex].Cells[conIndex];
            var tex = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return tex;
        }

        /// <summary>
        /// 登录数据库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                FillData();
            }
        }

        /// <summary>
        /// 给新增添加一行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            DataTable data = new DataTable();

            data.Columns.Add(new DataColumn("Id"));
            data.Columns.Add(new DataColumn("Title"));
            data.Columns.Add(new DataColumn("Content"));
            data.Columns.Add(new DataColumn("Author"));

            foreach(GridViewRow row in GridView1.Rows)
            {
                var temp = data.NewRow();

                temp["Id"] = GetValues(row.RowIndex, 0);
                temp["Title"] = GetValues(row.RowIndex, 1);
                temp["Content"] = GetValues(row.RowIndex, 2);
                temp["Author"] = GetValues(row.RowIndex, 3);

                data.Rows.Add(temp);
            }

            var tempIndex = data.NewRow();
            data.Rows.Add(tempIndex);

            GridView1.DataSource = data;
            GridView1.DataBind();

            GridView1.EditIndex = data.Rows.Count - 1;

            GridView1.DataSource = data;
            GridView1.DataBind();
        }

        /// <summary>
        /// 取消编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }

        /// <summary>
        /// 删除编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GetValues(e.RowIndex, 0));
            var sql = string.Format("delete from Articles where Id= {0}", id);

            DbHelper.AddOrUpdateOrDelete(sql);

            FillData();
        }
        /// <summary>
        /// 编辑模式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var id = GetValues(e.RowIndex, 0).Replace("&nbsp;", "");
            var title = GetValues(e.RowIndex, 1);
            var content = GetValues(e.RowIndex, 2);
            var author = GetValues(e.RowIndex, 3);
            
            //添加语句
            if (string.IsNullOrEmpty(id))
            {
                var sql = string.Format("insert into Articles (Title,Content,Author) values ('{0}','{1}','{2}')", title, content, author);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            //更新语句
            else
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}' where Id={3} ", title, content, author, id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }

            GridView1.EditIndex = -1;

            FillData();
        }
    }
}