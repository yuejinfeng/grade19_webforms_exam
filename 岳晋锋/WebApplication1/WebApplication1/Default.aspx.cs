﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                FillData();
            }
        }
        private void FillData()
        {
            var sql = "select * from Articles";
            var dt = DbHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = GetValue(e.RowIndex, 0);
            var sql = String.Format("delete from Articles where Id={0}",id);
            DbHelper.AddOrUpdateOrDelete(sql);
            FillData();
        }
        private string GetValue(int rowIndex,int colIndex)
        {
            var control = GridView1.Rows[rowIndex].Cells[colIndex];
            var dt = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return dt;
            
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var id = GetValue(e.RowIndex, 0).Replace("&nbsp", "");
            var title = GetValue(e.RowIndex, 1);
            var content = GetValue(e.RowIndex, 2);
            if (string.IsNullOrEmpty(id))
            {
                var sql = string.Format("insert into Articles(Title,Content,)Values('{0}','{1}')", Title,content);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            else
            {
                var sql = string.Format("update Articles set title='{0}',content='{1}' where id={2}", title, content,  id);
                DbHelper.AddOrUpdateOrDelete(sql);
            }
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var DataTable = CloneDataFromGrridView();
            GridView1.DataSource = DataTable;
            GridView1.DataBind();
            GridView1.EditIndex = DataTable.Rows.Count - 1;
            GridView1.DataSource = DataTable;
            GridView1.DataBind();
        }
        private DataTable CloneDataFromGrridView()
        {
            var DataTable = new DataTable();
            DataTable.Columns.Add(new DataColumn("Id"));
            DataTable.Columns.Add(new DataColumn("title"));
            DataTable.Columns.Add(new DataColumn("content"));
            foreach (GridViewRow row in GridView1.Rows) 
            {
                var tempRow = DataTable.NewRow();
                tempRow["Id"] = GetValue(row.RowIndex, 0);
                tempRow["title"] = GetValue(row.RowIndex, 1);
                tempRow["content"] = GetValue(row.RowIndex, 2);

                DataTable.Rows.Add(tempRow);
            }
            var temp = DataTable.NewRow();
                
            DataTable.Rows.Add(DataTable.NewRow());
            return DataTable;
        }
        
        }
    }
