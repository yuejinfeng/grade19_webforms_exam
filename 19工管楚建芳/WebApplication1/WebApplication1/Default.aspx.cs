﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Filldata();
            }
        }
        private void Filldata()
        {
            var sql = "select * from Articles ";
            var dt = DbHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();

        }
        private string GetwithRowIndedxColIndex(int rowIndex,int colIndex)
        {
            var cont = GridView1.Rows[rowIndex].Cells[colIndex];
            var value = cont.Controls.Count > 0 ? ((TextBox)cont.Controls[0]).Text : cont.Text;
            return value;

        }
        private DataTable ColneDataDromGridviem()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));

            foreach(GridViewRow row in GridView1.Rows)
            {
                var tempvalue = dt.NewRow();
                tempvalue["Id"] =GetwithRowIndedxColIndex(row.RowIndex, 0);
                tempvalue["Title"] = GetwithRowIndedxColIndex(row.RowIndex, 1);
               tempvalue["Content"] = GetwithRowIndedxColIndex(row.RowIndex, 2);
                tempvalue["Author"] = GetwithRowIndedxColIndex(row.RowIndex, 3);

                dt.Rows.Add(tempvalue);
            }
            dt.Rows.Add(dt.NewRow());

            return dt;
        }


        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var dataTable = ColneDataDromGridviem();
            GridView1.DataSource = dataTable;
            GridView1.DataBind();

            GridView1.EditIndex = dataTable.Rows.Count - 1;
            GridView1.DataSource = dataTable;
            GridView1.DataBind();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = GetwithRowIndedxColIndex(e.RowIndex, 0);
            var sql = string.Format("delete from Articles where Id={0}", id);
            DbHelper.GetDataTable(sql);
            Filldata();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var Title = GetwithRowIndedxColIndex(e.RowIndex, 1);
            var Content = GetwithRowIndedxColIndex(e.RowIndex, 2);
            var Author = GetwithRowIndedxColIndex(e.RowIndex, 3);
            var temp = GetwithRowIndedxColIndex(e.RowIndex, 0);
            var isok = int.TryParse(temp, out int id);

            if(isok)
            {
                var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}' where Id={3}", Title, Content, Author, id);
                DbHelper.GetDataTable(sql);
            }
            else
            {
                var sql = string.Format("insert into Articles (Title,Content,Author) values ('{0}','{1}','{2}')", Title, Content, Author);
                DbHelper.GetDataTable(sql);
            }
            GridView1.EditIndex = -1;
            Filldata();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            Filldata();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            Filldata();
        }
    }
}