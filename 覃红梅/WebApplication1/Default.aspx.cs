﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        private void FillData()
        {
            var sql = string.Format("select * from Articles");
            var dt = DbHelper.GetDataTable(sql);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        private string DataValue(int rowIndwx,int colIndex)
        {
            var count = GridView1.Rows[rowIndwx].Cells[colIndex];
            var text = count.Controls.Count > 0 ? ((TextBox)count.Controls[0]).Text : count.Text;
            return text;
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(DataValue(e.RowIndex, 0));
            var sql = string.Format("delete Articles where id={0}", id);
            DbHelper.GetDataTable(sql);
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillData();
        }
        private DataTable CloneGetValueFromGridValue()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("Id"));
            dataTable.Columns.Add(new DataColumn("Author"));
            dataTable.Columns.Add(new DataColumn("Title"));
            dataTable.Columns.Add(new DataColumn("Content"));
            foreach(GridViewRow row in GridView1.Rows)
            {
                var dt = dataTable.NewRow();
                dt["Id"] = DataValue(row.RowIndex, 0);
                dt["Author"] = DataValue(row.RowIndex, 1);
                dt["Title"] = DataValue(row.RowIndex, 2);
                dt["Content"] = DataValue(row.RowIndex, 3);
                dataTable.Rows.Add(dt);
            }

            dataTable.Rows.Add(dataTable.NewRow());
            return dataTable;

        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var dt = DataValue(e.RowIndex, 0);
            var Author = DataValue(e.RowIndex, 1);
            var Title = DataValue(e.RowIndex, 2);
            var Content = DataValue(e.RowIndex, 3);
            var isok = int.TryParse(dt, out int id);
            if (isok)
            { 
                    var sql = string.Format("Update  Articles set Title='{0}' ,Content='{1}',Author='{2}' where id = {3} ", Title, Content,Author, id);
                    DbHelper.AddOrUpdateOrDelete(sql);
                }
                else
                {
                    var sql = string.Format("insert into Articles (Content,Title,Author) Values ('{0}','{1}','{2}') ", Title, Content, Author);
                    DbHelper.AddOrUpdateOrDelete(sql);
                }
            GridView1.EditIndex = -1;
            FillData();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            var dataTable = CloneGetValueFromGridValue();
            GridView1.DataSource = dataTable;
            GridView1.DataBind();
            GridView1.EditIndex = dataTable.Rows.Count - 1;
            GridView1.DataSource = dataTable;
            GridView1.DataBind();
        }
    }
}


           
            
           