﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th{
            text-align:center;
            width:500px;
        }
        dt{
            color:blue;
            background-color:Highlight;
            text-align:center;
        }
    </style>

    <asp:Button runat="server"  Width="100px" Height="40px" BorderColor="DarkSlateBlue" Text="添加" OnClick="Unnamed_Click"/>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
        <Columns>
            <asp:BoundField DataField="Id" ReadOnly="true" HeaderText="Id" />
            <asp:BoundField DataField="Author" HeaderText="作者" />
            <asp:BoundField DataField="Title" HeaderText="标题" />
            <asp:BoundField DataField="Content" HeaderText="目录" />
            <asp:CommandField HeaderText="操作" ShowDeleteButton="True" ShowEditButton="True" ShowHeader="True" />
        </Columns>
    </asp:GridView>


</asp:Content>
