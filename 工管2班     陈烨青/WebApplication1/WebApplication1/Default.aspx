﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        tr {
            width:100px;
        text-align:center;
        background-color:lightblue;
        
        }
        th {
        
        text-align:center;
        width:200px;
        }
        body {
        
        width:250px;
        margin:50px auto;
       }
    </style>
    <asp:Button runat="server" Text="新增" OnClick="Unnamed_Click" BackColor="#66ff99" />

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">

        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" />
            <asp:BoundField DataField="Title" HeaderText="标题" />
            <asp:BoundField DataField="Content" HeaderText="内容" />
            <asp:BoundField DataField="Author" HeaderText="作者" />

            <asp:CommandField ButtonType="Button" HeaderText="操作" ShowDeleteButton="True" ShowEditButton="True" ShowHeader="True" />

        </Columns>
    </asp:GridView>

</asp:Content>
