﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                FillData();
            }
        }
        public void FillData() {
            var sql = "select Id,Title,Content,Author from Articles";
            var dt = DbHelper.GetDataTable(sql);
            GV1.DataSource = dt;
            GV1.DataBind();
        }
        public string GetValue(int row, int col) {
            var control = GV1.Rows[row].Cells[col];
            var txt = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return txt;
        }

        protected void GV1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GV1.EditIndex = -1;
            FillData();
        }

        protected void GV1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = int.Parse(GetValue(e.RowIndex, 0));
            var sql = string.Format("delete from Articles where id={0}", id);
            DbHelper.AddOrUpdateOrDelete(sql);
            GV1.EditIndex = -1;
            FillData();
        }

        protected void GV1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GV1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void GV1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var ok = int.TryParse(GetValue(e.RowIndex, 0), out int id);
            var title = GetValue(e.RowIndex, 1);
            var content = GetValue(e.RowIndex, 2);
            var author = GetValue(e.RowIndex, 3);
            if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(content) || string.IsNullOrEmpty(author))
            {
                Response.Write("<script language='javascript'>");
                Response.Write("alert('该值不能为空')");
                Response.Write("</script>");
                GV1.EditIndex = -1;
                FillData();
            }
            else
            {
                if (ok) {
                    var sql = string.Format("update Articles set Title='{0}',Content='{1}',Author='{2}' where Id={3}", title, content, author, id);
                    DbHelper.AddOrUpdateOrDelete(sql);
                    GV1.EditIndex = -1;
                    FillData();
                }
                else
                {
                    var sql = string.Format("insert into Articles (Title,Content,Author) values ('{0}','{1}','{2}')", title, content, author);
                    DbHelper.AddOrUpdateOrDelete(sql);
                    GV1.EditIndex = -1;
                    FillData();
                }
            }
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Title"));
            dt.Columns.Add(new DataColumn("Content"));
            dt.Columns.Add(new DataColumn("Author"));
            
            foreach (GridViewRow row in GV1.Rows) {
                var item = dt.NewRow();
                item["Id"] = GetValue(row.RowIndex, 0);
                item["Title"] = GetValue(row.RowIndex,1);
                item["Content"] = GetValue(row.RowIndex, 2);
                item["Author"] = GetValue(row.RowIndex, 3);
                dt.Rows.Add(item);
            }
            var tempitem = dt.NewRow();
             dt.Rows.Add(tempitem);
            GV1.DataSource = dt;
            GV1.DataBind();
            GV1.EditIndex = dt.Rows.Count - 1;
            GV1.DataSource = dt;
            GV1.DataBind();
        }
       
    }
}