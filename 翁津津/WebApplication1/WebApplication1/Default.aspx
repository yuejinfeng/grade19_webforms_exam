﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th {
            text-align: center;
            width: 120px;
            background-color: deepskyblue;
        }
        td{
            background-color:lightblue;
        }
        body
        {
            background-color:azure;
        }
    </style>
    <asp:Button runat="server" Text="新增" OnClick="Unnamed_Click" Width="69px" BackColor="DeepSkyBlue" />
    <asp:GridView runat="server" ID="GridView1" AutoGenerateColumns="false" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" />
            <asp:BoundField DataField="Title" HeaderText="标题" />
            <asp:BoundField DataField="Content" HeaderText="内容" />
            <asp:BoundField DataField="Author" HeaderText="作者"  />
            <asp:CommandField ShowDeleteButton="true" ShowEditButton="true" HeaderText="命令操作" />
        </Columns>
    </asp:GridView>

</asp:Content>
